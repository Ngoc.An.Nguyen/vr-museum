using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour
{
    public void Moderne()
    {
        SceneManager.LoadScene("01 Modern Raum");
    }

    public void Alt()
    {
        SceneManager.LoadScene("Alt Raum");
    }

    public void R4()
    {
        SceneManager.LoadScene("Raum 4");
    }

    public void KI()
    {
        SceneManager.LoadScene("KI Raum");
    }

    public void CubeWorld()
    {
        SceneManager.LoadScene("WuerfelWelt");
    }
 
}
